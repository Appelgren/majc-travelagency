/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.flightapplication.service;

import edu.chl.majc.flightapplication.Flight;
import edu.chl.majc.flightapplication.Flight_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * FlightFacadeREST
 * Publishing REST for other web applications to use.
 * @author matapp
 */
@Stateless
@Path("flight")
public class FlightFacadeREST extends AbstractFacade<Flight> {

    @PersistenceContext(unitName = "flights_pu")
    private EntityManager em;

    public FlightFacadeREST() {
        super(Flight.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Flight entity) {
        super.create(entity);
    }

    @PUT
    @Override
    @Consumes({"application/xml", "application/json"})
    public void edit(Flight entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Flight find(@PathParam("id") String id) {
        return super.find(id);
    }

    @GET
    @Path("from/{from}/to/{to}/date{date}")
    @Produces({"application/xml", "application/json"})
    public List<Flight> findFlight(@PathParam("from") String from, @PathParam("to") String to, @PathParam("date") String date) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Flight> cq = builder.createQuery(Flight.class);
        Root<Flight> flight = cq.from(Flight.class);
        
        Predicate p1 = builder.equal(flight.get("fromAirport"), from);
        Predicate p2 = builder.equal(flight.get("toAirport"), to);
        cq.where(builder.and(p1, p2));
        return em.createQuery(cq).getResultList();
    }

    @GET
    @Path("to/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Flight> findFlight(@PathParam("to") String to) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Flight> cq = builder.createQuery(Flight.class);

        Root<Flight> flight = cq.from(Flight.class);
        //Predicate p1 = builder.equal(flight.get("fromAirport"), from);
        Predicate p2 = builder.equal(flight.get("toAirport"), to);
        cq.where(p2);
        return em.createQuery(cq).getResultList();
    }
    
      @GET
    @Path("to/{to}/date/{date}")
    @Produces({"application/xml", "application/json"})
    public List<Flight> findFlight(@PathParam("to") String to, @PathParam("date") String date) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Flight> cq = builder.createQuery(Flight.class);
        Root<Flight> flight = cq.from(Flight.class);
       
        Predicate p2 = builder.equal(flight.get("toAirport"), to);
        
        Expression<String> p = flight.get(Flight_.depTime);
        Expression<String> exp1 = builder.substring(p,1,10);
        
        Predicate p3 = builder.equal(exp1, date);
        
        cq.where(builder.and(p2,p3));
        return em.createQuery(cq).getResultList();
    }

   /* @GET
    @Path("destinations")
    @Produces({"application/xml"})
    public Response getDestinations() {
    CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
    Root<Flight> flight = cq.from(Flight.class);
    javax.persistence.criteria.Path<String> destination = flight.get(Flight_.toAirport);
    cq.select(destination).distinct(true);
    List<String> res = em.createQuery(cq).getResultList();
    return Response.ok(em.createQuery(cq).getResultList()).build();
    //return em.createQuery(cq).getResultList(); 
    }*/
    
    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Flight> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Flight> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @java.lang.Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
