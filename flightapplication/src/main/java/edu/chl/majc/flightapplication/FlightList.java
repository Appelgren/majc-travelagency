/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.flightapplication;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author matapp
 */
@XmlType
public class FlightList {
    @XmlElement(required = true)
    private List<Flight> flights;

    public FlightList() {
    }

    public FlightList(List<Flight> flights) {
        this.flights = flights;
    }

    public List<Flight> getFlights() {
        return flights;
    }
    
}
