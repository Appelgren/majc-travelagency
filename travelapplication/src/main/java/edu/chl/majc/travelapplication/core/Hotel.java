/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.core;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author branting
 */
@Entity
@Table(name = "HOTELS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hotel.findAll", query = "SELECT h FROM Hotel h"),
    @NamedQuery(name = "Hotel.findByHotelId", query = "SELECT h FROM Hotel h WHERE h.hotelId = :hotelId"),
    @NamedQuery(name = "Hotel.findByName", query = "SELECT h FROM Hotel h WHERE h.name = :name"),
    @NamedQuery(name = "Hotel.findBySingleroomPrice", query = "SELECT h FROM Hotel h WHERE h.singleroomPrice = :singleroomPrice"),
    @NamedQuery(name = "Hotel.findByDescription", query = "SELECT h FROM Hotel h WHERE h.description = :description"),
    @NamedQuery(name = "Hotel.findByDoubleroomPrice", query = "SELECT h FROM Hotel h WHERE h.doubleroomPrice = :doubleroomPrice")})
public class Hotel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "HOTEL_ID")
    private Long hotelId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Min(value = 0)
    @Column(name = "SINGLEROOM_PRICE")
    private double singleroomPrice;
    @Size(max = 200)
    @Column(name = "DESCRIPTION")
    private String description;
    @NotNull
    @Min(value = 0)
    @Column(name = "DOUBLEROOM_PRICE")
    private Double doubleroomPrice;
    @NotNull
    @Size(min = 1, max = 20)
    @Basic(optional = false)
    @Column(name = "CLOSEST_CITY")
    private String closestCity;
    @Column(name = "IMG_URI")
    private String imgURI;

    public Hotel() {
    }

    public Hotel(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Hotel(Long hotelId, String name, double singleroomPrice) {
        this.hotelId = hotelId;
        this.name = name;
        this.singleroomPrice = singleroomPrice;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSingleroomPrice() {
        return singleroomPrice;
    }

    public void setSingleroomPrice(double singleroomPrice) {
        this.singleroomPrice = singleroomPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDoubleroomPrice() {
        return doubleroomPrice;
    }

    public void setDoubleroomPrice(Double doubleroomPrice) {
        this.doubleroomPrice = doubleroomPrice;
    }

    public String getClosestCity() {
        return closestCity;
    }

    public void setClosestCity(String closestCity) {
        this.closestCity = closestCity;
    }

    public String getImgURI() {
        return imgURI;
    }

    public void setImgURI(String imgURI) {
        this.imgURI = imgURI;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hotelId != null ? hotelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hotel)) {
            return false;
        }
        Hotel other = (Hotel) object;
        if ((this.hotelId == null && other.hotelId != null) || (this.hotelId != null && !this.hotelId.equals(other.hotelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.chl.majc.travelapplication.DB.Hotel[ hotelId=" + hotelId + " ]";
    }
    
}
