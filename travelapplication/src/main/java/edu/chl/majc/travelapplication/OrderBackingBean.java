/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.core.*;
import edu.chl.majc.travelapplication.communication.FlightUtilityBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;

/**
 *
 * @author root
 */
@ManagedBean(name = "orderBackingBean")
@CustomScoped(value = "#{window}")
public class OrderBackingBean {

    @ManagedProperty(value = "#{flightUtilityBean}")
    private FlightUtilityBean flightUtilityBean;
    
    private List<Flight> flights;
    private List<SelectItem> destinations;
    private String selectedDestination;
    private Flight bookedFlight;
    private Hotel bookedHotel;
    private Date date = Calendar.getInstance().getTime();
    private boolean renderFlights, flightBooked, hotelBooked;

    public FlightUtilityBean getFlightUtilityBean() {
        return flightUtilityBean;
    }

    public void setFlightUtilityBean(FlightUtilityBean flightUtilityBean) {
        this.flightUtilityBean = flightUtilityBean;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public boolean isRenderFlights() {
        return renderFlights;
    }

    public void setRenderFlights(boolean renderFlights) {
        this.renderFlights = renderFlights;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSelectedDestination() {
        return selectedDestination;
    }

    public void setSelectedDestination(String selectedDestination) {
        this.selectedDestination = selectedDestination;
    }

    public List<SelectItem> getDestinations() {
        destinations = new ArrayList<SelectItem>();
        for (String s : flightUtilityBean.getDestinations()) {
            destinations.add(new SelectItem(s));
        }
        return destinations;
    }

    public void setDestinations(List<SelectItem> destinations) {
        this.destinations = destinations;
    }

    public boolean isFlightBooked() {
        return flightBooked;
    }

    public void setFlightBooked(boolean flightBooked) {
        this.flightBooked = flightBooked;
    }

    public boolean isHotelBooked() {
        return hotelBooked;
    }

    public void setHotelBooked(boolean hotelBooked) {
        this.hotelBooked = hotelBooked;
    }

    public Flight getBookedFlight() {
        return bookedFlight;
    }

    public void setBookedFlight(Flight bookedFlight) {
        this.bookedFlight = bookedFlight;
    }

    public Hotel getBookedHotel() {
        return bookedHotel;
    }

    public void setBookedHotel(Hotel bookedHotel) {
        this.bookedHotel = bookedHotel;
    }
    
    

    /** Creates a new instance of TestBean */
    public OrderBackingBean() {
    }
}