/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.DB.JPAHotelController;
import edu.chl.majc.travelapplication.core.Hotel;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author branting
 */
@ManagedBean
@SessionScoped
public class HotelBackingBean {
    

    // Fields
    private String name;
    private double priceDouble;
    private double priceSingle;
    private String description;
    private String closestCity;
    private String imgURI;
    private boolean renderHotels;
    private String roomtype = "double";
    
    @ManagedProperty(value = "#{orderBackingBean}")
    OrderBackingBean obb;    
    @EJB
    JPAHotelController hc;
    
    /** Creates a new instance of HotelBackingBean */
    public HotelBackingBean() {
    }
    
    // Methods
    
    public List<Hotel> getHotels(){
        String city = obb.getBookedFlight().getToAirport();
        List<Hotel> hList = hc.findHotelsByCity(city);
        
        return hList;
    }

    public String getClosestCity() {
        return closestCity;
    }

    public void setClosestCity(String closestCity) {
        this.closestCity = closestCity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgURI() {
        return imgURI;
    }

    public void setImgURI(String imgURI) {
        this.imgURI = imgURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceDouble() {
        return priceDouble;
    }

    public void setPriceDouble(double priceDouble) {
        this.priceDouble = priceDouble;
    }

    public double getPriceSingle() {
        return priceSingle;
    }

    public void setPriceSingle(double priceSingle) {
        this.priceSingle = priceSingle;
    }

    public JPAHotelController getHc() {
        return hc;
    }

    public void setHc(JPAHotelController hc) {
        this.hc = hc;
    }

    public OrderBackingBean getObb() {
        return obb;
    }

    public void setObb(OrderBackingBean obb) {
        this.obb = obb;
    }

    public boolean isRenderHotels() {
        return renderHotels;
    }

    public void setRenderHotels(boolean renderHotels) {
        this.renderHotels = renderHotels;
    }

    public String getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(String roomtype) {
        this.roomtype = roomtype;
    }
    
    
}
