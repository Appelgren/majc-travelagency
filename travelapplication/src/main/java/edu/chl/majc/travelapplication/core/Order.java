/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.core;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "ORDERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Order.findAll", query = "SELECT o FROM Order o"),
    @NamedQuery(name = "Order.findByOrderId", query = "SELECT o FROM Order o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "Order.findByPrice", query = "SELECT o FROM Order o WHERE o.price = :price"),
    @NamedQuery(name = "Order.findByBookingDate", query = "SELECT o FROM Order o WHERE o.bookingDate = :bookingDate")})
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ORDER_ID")
    private String orderId;
    @Column(name = "PRICE")
    private Integer price;
    @Size(max = 14)
    @Column(name = "BOOKING_DATE")
    private String bookingDate;
    @JoinColumn(name = "DEP_FLIGHT", referencedColumnName = "FLIGHT_ID")
    @ManyToOne
    private Flight depFlight;
    @JoinColumn(name = "ARR_FLIGHT", referencedColumnName = "FLIGHT_ID")
    @ManyToOne
    private Flight arrFlight;

    public Order() {
    }

    public Order(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Flight getDepFlight() {
        return depFlight;
    }

    public void setDepFlight(Flight depFlight) {
        this.depFlight = depFlight;
    }

    public Flight getArrFlight() {
        return arrFlight;
    }

    public void setArrFlight(Flight arrFlight) {
        this.arrFlight = arrFlight;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Order)) {
            return false;
        }
        Order other = (Order) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.chl.wernvik.testproj.Orders[ orderId=" + orderId + " ]";
    }
    
}
