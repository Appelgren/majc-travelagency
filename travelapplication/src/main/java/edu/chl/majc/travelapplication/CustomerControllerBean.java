package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.core.Customer;
import edu.chl.majc.travelapplication.DB.JPACustomerController;
import edu.chl.majc.travelapplication.DB.JPATravelController;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;

/**
 *
 * @author matapp
 */
@ManagedBean(name = "customerControllerBean")
@SessionScoped
public class CustomerControllerBean {

    @ManagedProperty(value = "#{orderBackingBean}")
    private OrderBackingBean orderBackingBean;
    @ManagedProperty(value = "#{customerBackingBean}")
    private CustomerBackingBean customerBackingBean;
    private JPATravelController<Customer> c = new JPATravelController(Customer.class);
    @EJB
    private JPACustomerController cc;

    public CustomerBackingBean getCustomerBackingBean() {
        return customerBackingBean;
    }

    public void setCustomerBackingBean(CustomerBackingBean b) {
        this.customerBackingBean = b;
    }

    public OrderBackingBean getOrderBackingBean() {
        return orderBackingBean;
    }

    public void setOrderBackingBean(OrderBackingBean orderBackingBean) {
        this.orderBackingBean = orderBackingBean;
    }

    /** Creates a new instance of CustomerControllerBean */
    public CustomerControllerBean() {
    }

    public void createCustomer(ActionEvent evt) {
        Customer cust = new Customer(customerBackingBean.getName(), customerBackingBean.getPassword(), customerBackingBean.getName(), customerBackingBean.getEmail());
        cc.create(cust);
        customerBackingBean.setShowRegisterDialog(false);
        customerBackingBean.setActiveCustomer(cust);
    }

    public void login(ActionEvent e) {
        Customer c = cc.getByEmail(customerBackingBean.getEmail());
        System.out.println(c.getPassword());
        System.out.println(customerBackingBean.getPassword());
        if (c == null) {
            System.out.println("FINNS INTE");
        } else if (!c.getPassword().equals(customerBackingBean.getPassword())) {
            System.out.println("FEL PASS");
        } else {
            customerBackingBean.setActiveCustomer(c);
            setLoggedIn(true);
        }
    }
    
    public void logout(ActionEvent e){
        customerBackingBean.setActiveCustomer(null);
        setLoggedIn(false);
    }
    
    public void setLoggedIn(boolean b) {
        customerBackingBean.setShowLogin(!b);
        if(b){
               customerBackingBean.setLoginStatus("Logged in as " + customerBackingBean.getEmail());
        } else{
            customerBackingBean.setLoginStatus(null);
        }
        
    }

    public void showRegisterDialog(ActionEvent e) {
        customerBackingBean.setShowRegisterDialog(true);
        customerBackingBean.setAddress(null);
        customerBackingBean.setEmail(null);
        customerBackingBean.setName(null);
        customerBackingBean.setPassword(null);
    }

    public void cancelRegistration() {
        customerBackingBean.setShowRegisterDialog(false);
    }
    
    public void showOrderDialog(ActionEvent e){
        customerBackingBean.setShowOrderDialog(true);
    }
}
