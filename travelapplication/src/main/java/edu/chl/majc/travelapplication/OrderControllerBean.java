package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.communication.FlightUtilityBean;
import edu.chl.majc.travelapplication.core.*;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;

/**
 *
 * @author root
 */
@ManagedBean(name = "orderControllerBean")
@CustomScoped(value = "#{window}")
public class OrderControllerBean {
    @ManagedProperty(value = "#{orderBackingBean}")
    private OrderBackingBean orderBackingBean;
    @ManagedProperty(value = "#{hotelBackingBean}")
    private HotelBackingBean hotelBackingBean;
    @ManagedProperty(value = "#{flightUtilityBean}")
    private FlightUtilityBean flightUtilityBean;

    public FlightUtilityBean getFlightUtilityBean() {
        return flightUtilityBean;
    }

    public void setFlightUtilityBean(FlightUtilityBean flightUtilityBean) {
        this.flightUtilityBean = flightUtilityBean;
    }

    public void showFlights(ActionEvent e) {
        orderBackingBean.setFlights(flightUtilityBean.getFlights(orderBackingBean.getSelectedDestination()));
        orderBackingBean.setRenderFlights(true);
        hotelBackingBean.setRenderHotels(false);
    }

    public void chooseFlight(ActionEvent e) {
        /*TODO Get list of flights to destination and show them. */
        Flight f = (Flight) e.getComponent().getAttributes().get("flight");
        System.out.println(f.getFlightId());
        /*List<Flight> fl = new ArrayList<Flight>();
        fl.add(f);
        orderBackingBean.setFlights(fl);*/
        orderBackingBean.setFlightBooked(true);
        
        orderBackingBean.setBookedFlight(f);
        orderBackingBean.setRenderFlights(false);
        hotelBackingBean.setRenderHotels(true);
    }
    
    public void selectHotel(ActionEvent e){
        Hotel h = (Hotel) e.getComponent().getAttributes().get("Selected hotel");
        orderBackingBean.setBookedHotel(h);
        orderBackingBean.setHotelBooked(true);
        hotelBackingBean.setRenderHotels(false);
    }

    public OrderBackingBean getOrderBackingBean() {
        return orderBackingBean;
    }

    public void setOrderBackingBean(OrderBackingBean orderBackingBean) {
        this.orderBackingBean = orderBackingBean;
    }

    public HotelBackingBean getHotelBackingBean() {
        return hotelBackingBean;
    }

    public void setHotelBackingBean(HotelBackingBean hotelBackingBean) {
        this.hotelBackingBean = hotelBackingBean;
    }

    /** Creates a new instance of OrderControllerBean */
    public OrderControllerBean() {
    }
}
