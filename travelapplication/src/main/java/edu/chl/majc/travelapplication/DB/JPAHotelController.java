/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import edu.chl.majc.travelapplication.core.Hotel;
import edu.chl.majc.travelapplication.core.Hotel_;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author branting
 */
@Stateless
public class JPAHotelController implements Serializable {
    
    @PersistenceContext(unitName="travel_pu")
    private EntityManager em;
    
    public JPAHotelController(){
    }
    
    public Hotel find(String id){
        return em.find(Hotel.class, id);
    }
    
    public List<Hotel> findHotelsByCity(String city){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Hotel> cq = cb.createQuery(Hotel.class);
        Root<Hotel> root = cq.from(Hotel.class);
        cq.select(root);
        
        cq.where(cb.equal(root.get(Hotel_.closestCity), city));
        
        TypedQuery<Hotel> tq = em.createQuery(cq);        
        return tq.getResultList();
    }
    
}
