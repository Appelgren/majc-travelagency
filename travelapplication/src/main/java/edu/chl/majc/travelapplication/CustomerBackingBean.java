package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.core.Customer;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author matapp
 */
@ManagedBean(name = "customerBackingBean")
@SessionScoped
public class CustomerBackingBean {

    private String name;
    private String password;
    private String address;
    private String email;
    private String userStatus;
    private String loginStatus;
    private boolean showRegisterDialog = false, showOrderDialog = false;

    public boolean isShowOrderDialog() {
        return showOrderDialog;
    }

    public void setShowOrderDialog(boolean showOrderDialog) {
        this.showOrderDialog = showOrderDialog;
    }

    public boolean isShowRegisterDialog() {
        return showRegisterDialog;
    }

    public void setShowRegisterDialog(boolean showRegisterDialog) {
        this.showRegisterDialog = showRegisterDialog;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }
    private Customer activeCustomer;
    private boolean showLogin = true;

    public boolean isShowLogin() {
        return showLogin;
    }

    public void setShowLogin(boolean showLogin) {
        this.showLogin = showLogin;
    }

    public String getUserStatus() {
        if (activeCustomer == null) {
            userStatus = "Not logged in";
        } else {
            userStatus = "Logged in as " + activeCustomer.getEmail();
        }
        return userStatus;
    }

    public Customer getActiveCustomer() {
        return activeCustomer;
    }

    public void setActiveCustomer(Customer activeCustomer) {
        this.activeCustomer = activeCustomer;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CustomerBackingBean() {
    }

    public void validateName(FacesContext context, UIComponent validate, Object value) {
        String n = (String) value;

        if (!(5 < n.length() && n.length() < 30)) {
            ((UIInput) validate).setValid(false);
            FacesMessage msg = new FacesMessage("Size must be between 6-30");
            context.addMessage(validate.getClientId(context), msg);
        } else {
            ((UIInput) validate).setValid(true);
        }
    }

    public void validatePassword(FacesContext context, UIComponent validate, Object value) {
        String n = (String) value;

        if (!(5 < n.length() && n.length() < 30)) {
            ((UIInput) validate).setValid(false);
            FacesMessage msg = new FacesMessage("Size must be between 6-30");
            context.addMessage(validate.getClientId(context), msg);
        } else {
            ((UIInput) validate).setValid(true);
        }
    }

    public void validateEmail(FacesContext context, UIComponent validate, Object value) {
        String n = (String) value;

        if (n.contains("@")) {
            ((UIInput) validate).setValid(true);

        } else {
            ((UIInput) validate).setValid(false);
            FacesMessage msg = new FacesMessage("Email must contain an @");
            context.addMessage(validate.getClientId(context), msg);
        }
    }

    public void validateAddress(FacesContext context, UIComponent validate, Object value) {
        String n = (String) value;

        if (n.contains("0") || n.contains("1") || n.contains("2") || n.contains("3") || n.contains("4") || n.contains("5") || n.contains("6") || n.contains("7") || n.contains("8") || n.contains("9")) {
            ((UIInput) validate).setValid(true);
        } else {
            ((UIInput) validate).setValid(false);
            FacesMessage msg = new FacesMessage("Address must contain a number");
            context.addMessage(validate.getClientId(context), msg);
        }
    }
}
