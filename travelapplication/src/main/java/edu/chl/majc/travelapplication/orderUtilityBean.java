/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author branting
 */
@ManagedBean(name="orderUtilityBean")
@RequestScoped
public class OrderUtilityBean {

    // Fields
    @ManagedProperty(value = "#{orderBackingBean}")
    OrderBackingBean obb;
    @ManagedProperty(value = "#{hotelBackingBean}")
    HotelBackingBean hbb;
    
    // Methods
    public String getCalculateTotalHotelPrice(){
        double total;
        
        // Should be calculated based on outbound and return dates of flight
        int nrOfDays = 7;
        
        if (hbb.getRoomtype().equalsIgnoreCase("double")){
            total = nrOfDays * obb.getBookedHotel().getDoubleroomPrice();
            return "" + total;
        } else {
            total = nrOfDays * obb.getBookedHotel().getSingleroomPrice();
            return "" + total;
        }
    }
    
    public int getCalculateNrOfNights(){
        //TODO
        return 0;
                
    }

    public HotelBackingBean getHbb() {
        return hbb;
    }

    public void setHbb(HotelBackingBean hbb) {
        this.hbb = hbb;
    }

    public OrderBackingBean getObb() {
        return obb;
    }

    public void setObb(OrderBackingBean obb) {
        this.obb = obb;
    }
    
    /** Creates a new instance of orderUtilityBean */
    public OrderUtilityBean() {
    }
    
    
}
