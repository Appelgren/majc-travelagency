/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import junit.framework.Assert;
import javax.persistence.EntityTransaction;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ejb.EJB;
import javax.persistence.PersistenceContext;
import edu.chl.majc.travelapplication.core.Customer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matapp
 */
public class JPACustomerControllerTest {

    private static EntityManagerFactory emf;
    private EntityManager em;
    private JPACustomerController jcc;

    public JPACustomerControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        emf = Persistence.createEntityManagerFactory("test_travel_pu");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        emf.close();
    }

    @Before
    public void setUp() {
        em = emf.createEntityManager();
        jcc = new JPACustomerController();
        jcc.setEntitymanager(em);
    }

    @After
    public void tearDown() {
        em.close();
    }

    /**
     * Test of create method, of class JPACustomerController.
     */
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2 = null;

        Assert.assertFalse(em.contains(c1));

        jcc.create(c1);

        Assert.assertTrue(em.contains(c1));

        c2 = em.find(Customer.class, c1.getCustomerId());

        Assert.assertTrue(em.contains(c2));
        Assert.assertTrue(c1 == c2);
        Assert.assertTrue(c1.equals(c2));
        em.detach(c1);
        Assert.assertFalse(em.contains(c1));
    }

    /**
     * Test of edit method, of class JPACustomerController.
     */
    @Test
    public void testEdit() throws Exception {
        System.out.println("edit");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2 = c1;
        jcc.create(c1);
        Assert.assertTrue(c1 == c2);

        Assert.assertTrue(em.contains(c1));
        Assert.assertTrue(em.contains(c2));
        em.clear();
        Assert.assertFalse(em.contains(c1));
        Assert.assertFalse(em.contains(c2));

        c1.setPassword("FFFF");

        jcc.edit(c1);
        c1 = em.find(Customer.class, c1.getCustomerId());

        Assert.assertFalse(c1 == c2); // b1 *NOT* same reference

        Assert.assertTrue(c1.equals(c2)); // Same value
    }

    /**
     * Test of remove method, of class JPACustomerController.
     */
    @Test
    public void testRemove() throws Exception {
        System.out.println("remove");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2 = c1;
        jcc.create(c1);

        // Dont' need the data only the reference to remove
        c2 = em.getReference(Customer.class, c1.getCustomerId());
        jcc.remove(c2);

        Assert.assertFalse(em.contains(c2));
        Assert.assertFalse(em.contains(c1)); // Object identity!

        c2 = em.find(Customer.class, c1.getCustomerId());
        Assert.assertTrue(c2 == null);
    }

    /**
     * Test of find method, of class JPACustomerController.
     */
    @Test
    public void testFind() throws Exception {
        System.out.println("find");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2 = c1;

        jcc.create(c1);


        Assert.assertTrue(em.contains(jcc.find(c1.getCustomerId())));

        Assert.assertTrue(null == jcc.find("LONG STRING NOT AN ID"));

    }
    
        /**
     * Test of findAll method, of class JPACustomerController.
     */
    //@Test
    public void testFindAll() throws Exception {
        //TODO Does not work for some reason!?!
        System.out.println("findAll");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2 = new Customer("Hannah Montana", "xxx", "Drive 3", "miley@example.com");

        jcc.create(c1);
        jcc.create(c2);
        
        Assert.assertTrue(em.contains(c1));
        Assert.assertTrue(em.contains(c2));
        List<Customer> clist = jcc.findAll();

        System.out.println(clist.size());
        for(Customer c : clist){
            System.out.println(c.getName());
        }
        assertTrue(clist.contains(c1));
        assertTrue(clist.contains(c2));

    }

    /**
     * Test of getByEmail method, of class JPACustomerController.
     */
    //@Test
    public void testGetByEmail() throws Exception {
        //TODO This test is not working of some reason.
        
        System.out.println("getByEmail");

        Customer c1 = new Customer("Tony Montana", "pw", "Street 1", "tony@example.com");
        Customer c2;

        jcc.create(c1);

        c2 = jcc.find(c1.getCustomerId());
        System.out.println("##" + c2.getEmail() + "##");

        System.out.println("$$" + c1.getEmail() + "$$");
        c2 = jcc.getByEmail(c1.getEmail());
        System.out.println(c2);
        System.out.println(c1);
        Assert.assertTrue(c1.equals(c2));

        Assert.assertTrue(null == jcc.getByEmail("another@email.com"));

    }
}
