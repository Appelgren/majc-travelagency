/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import edu.chl.majc.travelapplication.core.Hotel;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class JPAHotelControllerTest {

    private static EntityManagerFactory emf;
    private EntityManager em;
    private JPAHotelController jhc;

    public JPAHotelControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        emf = Persistence.createEntityManagerFactory("test_travel_pu");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        emf.close();
    }

    @Before
    public void setUp() {
        em = emf.createEntityManager();
        jhc = new JPAHotelController();
        jhc.setEntitymanager(em);
    }

    @After
    public void tearDown() {
        em.close();
    }

    /**
     * Test of create method, of class JPAHotelController.
     */
    //@Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Hotel entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        instance.create(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of edit method, of class JPAHotelController.
     */
    //@Test
    public void testEdit() throws Exception {
        System.out.println("edit");
        Hotel entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        instance.edit(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of remove method, of class JPAHotelController.
     */
    //@Test
    public void testRemove() throws Exception {
        System.out.println("remove");
        Hotel entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        instance.remove(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of find method, of class JPAHotelController.
     */
    //@Test
    public void testFind() throws Exception {
        System.out.println("find");
        String id = "";
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        Hotel expResult = null;
        Hotel result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAll method, of class JPAHotelController.
     */
    //@Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        List expResult = null;
        List result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findRange method, of class JPAHotelController.
     */
    //@Test
    public void testFindRange() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        JPAHotelController instance = (JPAHotelController) container.getContext().lookup("java:global/classes/JPAHotelController");
        List expResult = null;
        List result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of count method, of class JPAHotelController.
     */
    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        int expResult = 0;
        int result = jhc.count();
        assertEquals(expResult, result);
    }

    /**
     * Test of findHotelsByCity method, of class JPAHotelController.
     */
    @Test
    public void testFindHotelsByCity() throws Exception {
        String city = "";
        List expResult = new ArrayList<Hotel>();
        List result = jhc.findHotelsByCity(city);
        assertEquals(expResult, result);
    }
}
