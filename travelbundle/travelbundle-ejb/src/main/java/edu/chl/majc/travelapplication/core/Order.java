/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.core;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "ORDERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Order.findAll", query = "SELECT o FROM Order o"),
    @NamedQuery(name = "Order.findByOrderId", query = "SELECT o FROM Order o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "Order.findByPrice", query = "SELECT o FROM Order o WHERE o.price = :price"),
    @NamedQuery(name = "Order.findByBookingDate", query = "SELECT o FROM Order o WHERE o.bookingDate = :bookingDate")})
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ORDER_ID")
    private String orderId;
    @Column(name = "PRICE")
    private Double price;
    @Size(max = 30)
    @Column(name = "BOOKING_DATE")
    private String bookingDate;
    @JoinColumn(name = "DEP_FLIGHT", referencedColumnName = "FLIGHT_ID")
    @ManyToOne
    private Flight depFlight;
    @JoinColumn(name = "ARR_FLIGHT", referencedColumnName = "FLIGHT_ID")
    @ManyToOne
    private Flight arrFlight;
    @JoinColumn(name = "HOTEL", referencedColumnName = "HOTEL_ID")
    @ManyToOne
    private Hotel hotel;
    @Size(min = 6, max = 6)
    @Column(name = "ROOMTYPE")
    private String chosenRoomtype;
    @Column(name = "NR_OF_NIGHTS")
    private int nrOfNightsAtHotel;
    @JoinColumn(name = "CUSTOMER", referencedColumnName = "CUSTOMER_ID")
    private Customer customer;
    
    public Order() {
    }

    public Order(String orderId) {
        this.orderId = orderId;
    }
    
    public Order(Customer c, Flight f, Hotel h, String roomtype, int nrOfNights, double price){
        this.bookingDate = Calendar.getInstance().getTime().toString();
        this.customer = c;
        this.depFlight = f;
        this.price = price;
        if (h != null){
            this.hotel = h;
            this.chosenRoomtype = roomtype;
            this.nrOfNightsAtHotel = nrOfNights;
        }
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Flight getDepFlight() {
        return depFlight;
    }

    public void setDepFlight(Flight depFlight) {
        this.depFlight = depFlight;
    }

    public Flight getArrFlight() {
        return arrFlight;
    }

    public void setArrFlight(Flight arrFlight) {
        this.arrFlight = arrFlight;
    }

    public String getChosenRoomtype() {
        return chosenRoomtype;
    }

    public void setChosenRoomtype(String chosenRoomtype) {
        this.chosenRoomtype = chosenRoomtype;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getNrOfNightsAtHotel() {
        return nrOfNightsAtHotel;
    }

    public void setNrOfNightsAtHotel(int nrOfNightsAtHotel) {
        this.nrOfNightsAtHotel = nrOfNightsAtHotel;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Order)) {
            return false;
        }
        Order other = (Order) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.chl.wernvik.testproj.Orders[ orderId=" + orderId + " ]";
    }
    
}
