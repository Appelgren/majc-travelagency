/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import edu.chl.majc.travelapplication.core.Customer;
import edu.chl.majc.travelapplication.core.Customer_;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author lujoel
 */
@Stateless
public class JPACustomerController extends JPATravelController<Customer> {

    @PersistenceContext(unitName = "travel_pu")
    private EntityManager em;

    public JPACustomerController() {
        super(Customer.class);
    }

    public Customer getByEmail(String email) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
        Root<Customer> cu = query.from(Customer.class);
        query.where(builder.equal(cu.get(Customer_.email),email));
        javax.persistence.Query q = em.createQuery(query);
        try{
            return (Customer) q.getSingleResult();
        }catch(NoResultException e){
            return null;
        }catch(NonUniqueResultException e){
            return null;
        }
    }

    @Override
    protected EntityManager getEntitymanager() {
        return em;
    }

    @Override
    protected void setEntitymanager(EntityManager em) {
        this.em = em;
    }
}
