/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import edu.chl.majc.travelapplication.core.Flight;
import edu.chl.majc.travelapplication.core.Flight_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 *
 * @author lujoel
 */
@Stateless
public class JPAFlightController extends JPATravelController<Flight> {

    @PersistenceContext(unitName = "travel_pu")
    private EntityManager em;

    public JPAFlightController() {
        super(Flight.class);
    }

    @Override
    protected EntityManager getEntitymanager() {
        return em;
    }

    @Override
    protected void setEntitymanager(EntityManager em) {
        this.em = em;
    }
}
