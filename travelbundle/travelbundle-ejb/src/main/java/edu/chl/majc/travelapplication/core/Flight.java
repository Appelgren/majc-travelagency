/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.core;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author root
 */
@Entity
@Table(name = "FLIGHTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Flight.findAll", query = "SELECT f FROM Flight f"),
    @NamedQuery(name = "Flight.findByFlightId", query = "SELECT f FROM Flight f WHERE f.flightId = :flightId"),
    @NamedQuery(name = "Flight.findByDepTime", query = "SELECT f FROM Flight f WHERE f.depTime = :depTime"),
    @NamedQuery(name = "Flight.findByArrTime", query = "SELECT f FROM Flight f WHERE f.arrTime = :arrTime"),
    @NamedQuery(name = "Flight.findByFromAirport", query = "SELECT f FROM Flight f WHERE f.fromAirport = :fromAirport"),
    @NamedQuery(name = "Flight.findByToAirport", query = "SELECT f FROM Flight f WHERE f.toAirport = :toAirport")})
public class Flight implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "FLIGHT_ID")
    private String flightId;
    @Column(name = "PRICE")
    private int price;
    @Size(max = 50)
    @Column(name = "DEP_TIME")
    private String depTime;
    @Size(max = 50)
    @Column(name = "ARR_TIME")
    private String arrTime;
    @Size(max = 50)
    @Column(name = "FROM_AIRPORT")
    private String fromAirport;
    @Size(max = 50)
    @Column(name = "TO_AIRPORT")
    private String toAirport;

    public Flight() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Flight(String flightId, int price, String depTime, String arrTime, String fromAirport, String toAirport) {
        this.flightId = flightId;
        this.price = price;
        this.depTime = depTime;
        this.arrTime = arrTime;
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
    }

    public Flight(String flightId) {
        this.flightId = flightId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getDepTime() {
        return depTime;
    }

    public void setDepTime(String depTime) {
        this.depTime = depTime;
    }

    public String getArrTime() {
        return arrTime;
    }

    public void setArrTime(String arrTime) {
        this.arrTime = arrTime;
    }

    public String getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

    public String getToAirport() {
        return toAirport;
    }

    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (flightId != null ? flightId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flight)) {
            return false;
        }
        Flight other = (Flight) object;
        if ((this.flightId == null && other.flightId != null) || (this.flightId != null && !this.flightId.equals(other.flightId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.chl.majc.travelapplication.Flight[ flightId=" + flightId + " ]";
    }
    
}
