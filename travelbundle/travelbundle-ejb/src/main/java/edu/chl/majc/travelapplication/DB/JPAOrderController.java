/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.DB;

import edu.chl.majc.travelapplication.core.Order;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author wernvik
 */
@Stateless
public class JPAOrderController extends JPATravelController<Order>{
    
    @PersistenceContext(unitName = "travel_pu")
    private EntityManager em;
    
        public JPAOrderController() {
        super(Order.class);
    }

    @Override
    protected EntityManager getEntitymanager() {
        return em;
    }

    @Override
    protected void setEntitymanager(EntityManager em) {
        this.em = em;
    }
    
}
