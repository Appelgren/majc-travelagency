/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.administration;

import edu.chl.majc.travelapplication.DB.JPACustomerController;
import edu.chl.majc.travelapplication.core.Customer;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ValueChangeEvent;

/**
 * ControllerBean for administrating the customer in the administration
 * part of the application.
 * @author Joel
 */
@ManagedBean
@SessionScoped
public class AdminCustomersControllerBean {

    @EJB
    JPACustomerController cc;
    
    /** Creates a new instance of adminCustomersBackingBean */
    public AdminCustomersControllerBean() {
    }
    
    /**
     * Returns a list of existing customers
     * @return List<Customer>
     */
    public List<Customer> getCustomers() {

        return cc.findAll();
    }

    /*A desperate try to implement the persistance part
     *of the editable table but not enough time for this.
     The standard component should fix this but tried a
     and it didn't work out "copying" their way of doing it.*/
    public void nameChanged(ValueChangeEvent e) {

        Customer c = (Customer) e.getComponent().getAttributes().get("customer");
        c.setName((String) e.getNewValue());
        cc.edit(c);
    }

    public void addressChanged(ValueChangeEvent e) {

        Customer c = (Customer) e.getComponent().getAttributes().get("customer");
        c.setAddress((String) e.getNewValue());
        cc.edit(c);
    }

    public void emailChanged(ValueChangeEvent e) {

        Customer c = (Customer) e.getComponent().getAttributes().get("customer");
        c.setEmail((String) e.getNewValue());
        cc.edit(c);
    }

    public void passwordChanged(ValueChangeEvent e) {

        Customer c = (Customer) e.getComponent().getAttributes().get("customer");
        c.setPassword((String) e.getNewValue());
        cc.edit(c);
    }
}
