package edu.chl.majc.travelapplication.webbeans;

import edu.chl.majc.travelapplication.DB.JPAFlightController;
import edu.chl.majc.travelapplication.DB.JPAHotelController;
import edu.chl.majc.travelapplication.DB.JPAOrderController;
import edu.chl.majc.travelapplication.communication.FlightBean;
import edu.chl.majc.travelapplication.core.Customer;
import edu.chl.majc.travelapplication.core.Flight;
import edu.chl.majc.travelapplication.core.Hotel;
import edu.chl.majc.travelapplication.core.Order;
import javax.ejb.EJB;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author root
 */
@ManagedBean(name = "orderControllerBean")
@SessionScoped
public class OrderControllerBean {

    @ManagedProperty(value = "#{orderBackingBean}")
    private OrderBackingBean orderBackingBean;
    @ManagedProperty(value = "#{hotelBackingBean}")
    private HotelBackingBean hotelBackingBean;
    @ManagedProperty(value = "#{customerBackingBean}")
    private CustomerBackingBean customerBackingBean;
    @ManagedProperty(value = "#{orderUtilityBean}")
    private OrderUtilityBean orderUtilityBean;
    @EJB
    private FlightBean flightBean;
    @EJB
    private JPAFlightController jpafc;
    @EJB
    private JPAOrderController jpaoc;
    @EJB
    private JPAHotelController jpahc;

    public void showFlights(ActionEvent e) {
        orderBackingBean.setFlights(flightBean.getFlights(orderBackingBean.getSelectedDestination(), orderBackingBean.getDate()));
        orderBackingBean.setRenderFlights(true);
    }

    public void chooseFlight(ActionEvent e) {
        /*TODO Get list of flights to destination and show them. */
        Flight f = (Flight) e.getComponent().getAttributes().get("flight");
        System.out.println(f.getFlightId());
        /*List<Flight> fl = new ArrayList<Flight>();
        fl.add(f);
        orderBackingBean.setFlights(fl);*/
        orderBackingBean.setFlightBooked(true);
        orderBackingBean.setBookedFlight(f);
        orderBackingBean.setRenderFlights(false);
        orderBackingBean.setHotelBooked(false);
        hotelBackingBean.setRenderHotels(true);
    }

    public void selectHotel(ActionEvent e) {
        Hotel h = (Hotel) e.getComponent().getAttributes().get("Selected hotel");
        orderBackingBean.setBookedHotel(h);
        orderBackingBean.setHotelBooked(true);
        hotelBackingBean.setRenderHotels(false);
    }

    public void doCheckout(ActionEvent e) {
        //TODO: Fixa övergången till checkout
        /*Map<String, Object> attributesMap = e.getComponent().getAttributes();
        Flight f = (Flight) attributesMap.get("flight");
        Hotel h = (Hotel) attributesMap.get("hotel");
        String roomtype = (String) attributesMap.get("roomtype");*/
    }

    public String doConfirmBooking() {
        Hotel h = orderBackingBean.getBookedHotel();
        Customer c = customerBackingBean.getActiveCustomer();
        Flight f = orderBackingBean.getBookedFlight();
        String roomtype = hotelBackingBean.getRoomtype();
        int nrOfNights = 7;
        double price = orderUtilityBean.calculateTotalPrice();
        Flight f2 = new Flight("TDA067", 345, "2011-08-24", "2011-08-24", "New York", "Oakland");

        Order o = new Order(c, f, h, roomtype, nrOfNights, price);
        try {
            jpafc.create(f);
            jpaoc.create(o);
            return "success";
        } catch (Exception ex){
            
            return "failure";
        }
    }

    public OrderBackingBean getOrderBackingBean() {
        return orderBackingBean;
    }

    public void setOrderBackingBean(OrderBackingBean orderBackingBean) {
        this.orderBackingBean = orderBackingBean;
    }

    public HotelBackingBean getHotelBackingBean() {
        return hotelBackingBean;
    }

    public void setHotelBackingBean(HotelBackingBean hotelBackingBean) {
        this.hotelBackingBean = hotelBackingBean;
    }

    public CustomerBackingBean getCustomerBackingBean() {
        return customerBackingBean;
    }

    public void setCustomerBackingBean(CustomerBackingBean customerBackingBean) {
        this.customerBackingBean = customerBackingBean;
    }

    public OrderUtilityBean getOrderUtilityBean() {
        return orderUtilityBean;
    }

    public void setOrderUtilityBean(OrderUtilityBean orderUtilityBean) {
        this.orderUtilityBean = orderUtilityBean;
    }

    /** Creates a new instance of OrderControllerBean */
    public OrderControllerBean() {
    }
}
