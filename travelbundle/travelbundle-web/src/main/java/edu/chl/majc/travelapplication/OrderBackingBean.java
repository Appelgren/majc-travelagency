/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication;

import edu.chl.majc.travelapplication.communication.FlightBean;
import edu.chl.majc.travelapplication.core.Flight;
 
import edu.chl.majc.travelapplication.core.Hotel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;


/**
 *
 * @author root
 */
@ManagedBean(name = "orderBackingBean")
@CustomScoped(value = "#{window}")
public class OrderBackingBean {

    @EJB
    private FlightBean flightBean;
    private List<Flight> flights;
    private List<SelectItem> destinations;
    private String selectedDestination;
    private Flight bookedFlight;
    private Hotel bookedHotel;
    private Date date = Calendar.getInstance().getTime();
    private boolean renderFlights, flightBooked, hotelBooked;

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public boolean isRenderFlights() {
        return renderFlights;
    }

    public void setRenderFlights(boolean renderFlights) {
        this.renderFlights = renderFlights;
    }

    public Date getDate() {
        return (Date) date.clone();
    }

    public void setDate(Date date) {
        this.date = (Date) date.clone();
    }

    public String getSelectedDestination() {
        return selectedDestination;
    }

    public void setSelectedDestination(String selectedDestination) {
        this.selectedDestination = selectedDestination;
    }

    public List<SelectItem> getDestinations() {
        destinations = new ArrayList<SelectItem>();
        for (String s : flightBean.getDestinations()) {
            destinations.add(new SelectItem(s));
        }
        return destinations;
    }

    public void setDestinations(List<SelectItem> destinations) {
        this.destinations = destinations;
    }

    public boolean isFlightBooked() {
        return flightBooked;
    }

    public void setFlightBooked(boolean flightBooked) {
        this.flightBooked = flightBooked;
    }

    public boolean isHotelBooked() {
        return hotelBooked;
    }

    public void setHotelBooked(boolean hotelBooked) {
        this.hotelBooked = hotelBooked;
    }

    public Flight getBookedFlight() {
        return bookedFlight;
    }

    public void setBookedFlight(Flight bookedFlight) {
        this.bookedFlight = bookedFlight;
    }

    public Hotel getBookedHotel() {
        return bookedHotel;
    }

    public void setBookedHotel(Hotel bookedHotel) {
        this.bookedHotel = bookedHotel;
    }

    /** Creates a new instance of TestBean */
    public OrderBackingBean() {
    }
}
