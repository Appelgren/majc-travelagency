/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.webbeans;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author branting
 */
@ManagedBean(name = "orderUtilityBean")
@SessionScoped
public class OrderUtilityBean {

    // Fields
    @ManagedProperty(value = "#{orderBackingBean}")
    private OrderBackingBean obb;
    @ManagedProperty(value = "#{hotelBackingBean}")
    private HotelBackingBean hbb;

    // Methods
    public String getCalculateTotalHotelPrice() {
        return "" + calculateTotalHotelPrice();
    }

    public double calculateTotalHotelPrice() {
        double total = 0, hotelprice;
        // Should be calculated based on outbound and return dates of flight
        int nrOfDays = 7;

        if (obb.getBookedHotel() != null) {
            if (hbb.getRoomtype().equalsIgnoreCase("double")) {
                total = nrOfDays * obb.getBookedHotel().getDoubleroomPrice();
            } else if (hbb.getRoomtype().equalsIgnoreCase("single")) {
                total = nrOfDays * obb.getBookedHotel().getSingleroomPrice();
            }
            return total;
        } else {
            return total;
        }
    }
    
    public double calculateTotalPrice(){
        return calculateTotalHotelPrice() + obb.getBookedFlight().getPrice();
    }
    

    public String getCalculateTotalPrice() {
        return "" + calculateTotalPrice();
    }

    public int getCalculateNrOfNights() {
        //TODO
        return 0;

    }

    public HotelBackingBean getHbb() {
        return hbb;
    }

    public void setHbb(HotelBackingBean hbb) {
        this.hbb = hbb;
    }

    public OrderBackingBean getObb() {
        return obb;
    }

    public void setObb(OrderBackingBean obb) {
        this.obb = obb;
    }

    /** Creates a new instance of orderUtilityBean */
    public OrderUtilityBean() {
    }
}
