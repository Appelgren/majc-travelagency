package edu.chl.majc.travelapplication.webbeans;

import edu.chl.majc.travelapplication.core.Customer;
import edu.chl.majc.travelapplication.DB.JPACustomerController;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ActionEvent;
import javax.persistence.NoResultException;
import org.eclipse.persistence.exceptions.DatabaseException;

/**
 *
 * Handles everything that directly effects the customer, such as login and registration components
 * 
 * @author matapp
 */
@ManagedBean(name = "customerControllerBean")
@SessionScoped
public class CustomerControllerBean {

    @ManagedProperty(value = "#{orderBackingBean}")
    private OrderBackingBean orderBackingBean;
    @ManagedProperty(value = "#{customerBackingBean}")
    private CustomerBackingBean customerBackingBean;
    @EJB
    private JPACustomerController cc;

    public CustomerBackingBean getCustomerBackingBean() {
        return customerBackingBean;
    }

    public void setCustomerBackingBean(CustomerBackingBean b) {
        this.customerBackingBean = b;
    }

    public OrderBackingBean getOrderBackingBean() {
        return orderBackingBean;
    }

    public void setOrderBackingBean(OrderBackingBean orderBackingBean) {
        this.orderBackingBean = orderBackingBean;
    }

    /** Creates a new instance of CustomerControllerBean */
    public CustomerControllerBean() {
    }

    public void createCustomer(ActionEvent evt) {
        Customer cust = new Customer(customerBackingBean.getName(), customerBackingBean.getPassword(), customerBackingBean.getName(), customerBackingBean.getEmail());
        try {
            cc.create(cust);
            customerBackingBean.setUserExists(null);
            customerBackingBean.setShowRegisterDialog(false);
            customerBackingBean.setActiveCustomer(cust);
            setLoggedIn(true);
        }catch (Exception e) {
            //Not totally true, but is a possible problem.
            customerBackingBean.setUserExists("A user with that amil already exists.");        
        }

    }

    public void login(ActionEvent evt) {
        try {
            Customer c = cc.getByEmail(customerBackingBean.getEmail());

            if (c == null) {
                customerBackingBean.setLoginStatus("Couldn't find customer with that email address.");
            } else if (!c.getPassword().equals(customerBackingBean.getPassword())) {
                customerBackingBean.setLoginStatus("Wrong password.");
            } else {
                customerBackingBean.setActiveCustomer(c);
                setLoggedIn(true);
            }
        } catch (NoResultException e) {
            System.out.println("NoResultException");
        }

    }

    public void logout(ActionEvent e) {
        customerBackingBean.setActiveCustomer(null);
        setLoggedIn(false);
    }

    public void setLoggedIn(boolean b) {
        customerBackingBean.setShowLogin(!b);
        if (b) {
            customerBackingBean.setLoginStatus("Logged in as " + customerBackingBean.getEmail());
        } else {
            customerBackingBean.setLoginStatus(null);
        }

    }

    public void showRegisterDialog(ActionEvent e) {
        customerBackingBean.setShowRegisterDialog(true);
        customerBackingBean.setAddress(null);
        customerBackingBean.setEmail(null);
        customerBackingBean.setName(null);
        customerBackingBean.setPassword(null);
    }

    public void cancelRegistration() {
        customerBackingBean.setShowRegisterDialog(false);
    }

    public void showOrderDialog(ActionEvent e) {
        customerBackingBean.setShowOrderDialog(true);
    }
    
    public void cancelViewOrders(ActionEvent e) {
        customerBackingBean.setShowOrderDialog(false);
    }
}
