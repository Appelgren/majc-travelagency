/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.chl.majc.travelapplication.communication;

import edu.chl.majc.travelapplication.core.Flight;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * FlightBean
 * A stateless utility bean that takes care of getting results from a remote airline application.
 * 
 * @author matapp
 */
@Stateless
public class FlightBean {

    private final String FLIGHT_BASE_URI = "http://localhost:8080/flightapplication-1.0-SNAPSHOT/resources/flight/";

    /** Creates a new instance of FlightUtilityBean */
    public FlightBean() {
    }

    public List<Flight> getFlights(String to) {
        try {
            return unmarshalFlights(FLIGHT_BASE_URI + "to/" + to);
        } catch (MalformedURLException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new LinkedList<Flight>();
    }

    public List<Flight> getFlights(String to, Date date) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
     
            return unmarshalFlights(FLIGHT_BASE_URI + "to/" + to + "/date/" + new StringBuilder(df.format(date)));
        } catch (MalformedURLException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new LinkedList<Flight>();
    }

    public List<String> getDestinations() {
        List<Flight> flights;
        List<String> res = new LinkedList<String>();
        try {
            flights = unmarshalFlights(FLIGHT_BASE_URI);
            for (Flight f : flights) {
                if (!res.contains(f.getToAirport())) {
                    res.add(f.getToAirport());
                }
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(FlightBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;

    }

    private static List<Flight> unmarshalFlights(String uri) throws MalformedURLException, JAXBException {
        URL url = new URL(uri);
        JAXBContext jc = JAXBContext.newInstance(FlightList.class);
        Unmarshaller u = jc.createUnmarshaller();
        Object o = u.unmarshal(url);
        return ((FlightList) o).getFlights();
    }
}
